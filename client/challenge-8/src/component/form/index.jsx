import React from 'react'
import './style.css'

const ListName = ({ name, handleDelete, handleUpdate}) => {
    return(
        <tr className='form-container'>
            <td>{name.name}</td>
            <td>{name.email}</td>
            <td>{name.Exp}</td>
            <td>{name.lvl}</td>
            <button onClick={() => handleDelete(name)}>Delete</button>
            <button onClick={() => handleUpdate(name)}>Update</button>
        </tr>
    )
}

export default ListName;