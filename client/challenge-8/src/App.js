import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import Header from './component/header'
import Form from './component/form'

function App() {

  const [value, setValue] = useState("");
  const [Mailvalue, setMailValue] = useState("");
  const [Expvalue, setExpValue] = useState("");
  const [Lvlvalue, setLvlValue] = useState("");
  const [name, setName] = useState("");
  const [mail, setMail] = useState("");
  const [Exp, setExp] = useState("");
  const [Lvl, setLvl] = useState("");
  const [names, setNames] = useState([]);
  const [error, setError] = useState("")
  const [isUpdate, setIsUpdate] = useState(false)

  const handleSubmit = () => {
    const isFind = names.find((row) => row === name)
    if (!isUpdate) {
      if (!isFind) {
        setNames([...names, {name: value, email: Mailvalue, Exp: Expvalue, lvl: Lvlvalue}])
        setValue("")
        setMailValue("")
        setLvlValue("")
        setExpValue("")
        console.log(names)
      } else {
        setError("Name already taken!")
        setTimeout(() => {
          setError("");
        }, 1500)
      }
    } else {

      const newNames = names.map((row) => {
        return row === name ? value : row;
      })

      setName("");
      setMail("");
      setExp("");
      setLvl("");
      setNames(newNames)
      setIsUpdate(false)
    }
  }

  const NamehandleChange = (e) => {
    setValue(e.target.value)
  }

  const MailhandleChange = (e) => {
    setMailValue(e.target.value)
  }

  const ExphandleChange = (e) => {
    setExpValue(e.target.value)
  }

  const LvlhandleChange = (e) => {
    setLvlValue(e.target.value)
  }

  const handleDelete = (name) => {
    const newName = names.filter((row) => row !== name)

    setNames(newName)
  }

  const handleUpdate = (name) => {
    setIsUpdate(true)
    setName(name.name)
    setMail(name.email)
    setExp(name.Exp)
    setLvl(name.lvl)
    setValue(name.name)
    setMailValue(name.email)
    setExpValue(name.Exp)
    setLvlValue(name.lvl)
  }

  return (
    <div className="container">
      <div>
        <Header />

        <input value={value} onChange={NamehandleChange} placeholder="name"/>
        <input value={Mailvalue} onChange={MailhandleChange} placeholder="email" />
        <input value={Expvalue} onChange={ExphandleChange} placeholder="Exp" />
        <input value={Lvlvalue} onChange={LvlhandleChange} placeholder="Lvl" />
        {value && <button onClick={handleSubmit}>submit</button>}
        {error && <div className='error'>{error}</div>}

        <table className="table">
        <thead>
          <tr>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Exp</th>
            <th scope="col">Lvl</th>
          </tr>
        </thead>
        <tbody>
          {names.map((row, index) => (
            <Form name={row} key={index} handleDelete={handleDelete} handleUpdate={handleUpdate}/>
          ))}
        </tbody>
        </table>
        {names.length < 1 && <div className='empty'>Data Empty</div>}
      </div>

    </div>
  );
}

export default App;
